# вычислить среднее и стандартное отклонение набора изображений
# https://stackoverflow.com/questions/73350133/how-to-calculate-mean-and-standard-deviation-of-a-set-of-images
from PIL import Image
import numpy as np
from glob import glob

path = "train/1/*.jpg"
imgs_path = glob(path)


rgb_values = np.concatenate(
    [Image.open(img).getdata() for img in imgs_path], 
    axis=0
) / 255.

# rgb_values.shape == (n, 3), 
# where n is the total number of pixels in all images, 
# and 3 are the 3 channels: R, G, B.

# Each value is in the interval [0; 1]

mu_rgb = np.mean(rgb_values, axis=0)  # mu_rgb.shape == (3,)
std_rgb = np.std(rgb_values, axis=0)  # std_rgb.shape == (3,)
print(f'среднее = {mu_rgb} стандартное отклонение = {std_rgb}')
# print(f'среднее = {mu_rgb}')
# print(f'стандартное отклонение = {std_rgb}')