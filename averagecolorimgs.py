# conda activate iWM
# https://lifewithdata.com/2023/05/17/how-to-normalize-data-in-python/

import cv2
import numpy as np
from glob import glob

path = "train/1/*.jpg"
names = glob(path)

R = 0
G = 0
B = 0
n = 0

for name in names:
	src_img = cv2.imread(name)
	average_color_row = np.average(src_img, axis=0)
	average_color = np.average(average_color_row, axis=0)
	r, g, b = average_color
	print(f'R = {r}, G = {g}, B = {b}')
	R += r
	G += g
	B += b
	n += 1

print(f'R_means = {R/n}, G_means = {G/n}, B_means = {B/n}')

# d_img = np.ones((312, 312, 3), dtype=np.uint8)
# d_img[:, :] = average_color

# cv2.imshow("Source image", src_img)
# cv2.imshow("Average Color", d_img)
# cv2.waitKey(0)