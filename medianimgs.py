# https://stackoverflow.com/questions/46925896/create-an-image-using-the-pixels-median-fom-other-images

import cv2
import numpy as np
from glob import glob

path = "train/1/*.jpg"
names = glob(path)

imgs = []

for name in names:
	print(name)
	src_img = cv2.imread(name)
	imgs.append(src_img)

imgs = np.asarray(imgs)

# Take the median over the first dim
med = np.median(imgs)
print(med)

median_image = np.median(imgs, axis=0)
cv2.imwrite("median_image_1.jpg",median_image)